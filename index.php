<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <link href="login.css" rel="stylesheet">
    <link rel="icon" href="../img/icono.ico">
    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="css/clean-blog.min.css" rel="stylesheet">
    <!--FontAwesome-->
    <link href="css/all.css" rel="stylesheet"> <!--load all styles -->
    
  </head>
  <body style="background-image: url('img/test-1.jpg')">
  
  
<div class="login-page">
  <div class="form">
    <form class="login-form" action="authentication/authentication.php" method="post">
            
                    <?php
                    if(isset($_GET["status"])){
                        $id = $_GET["status"];
                        $message = $_GET["msg"];
                        echo"<br> <div id='$id' style='margin-left:-1%; margin-top:8%; text-align:center; color:red;'>$message</div>";
                    }?>
                        
      <input type="text" placeholder="usuario" name="username">
      <input type="password" placeholder="clave" name="password">
      <button class="btn btn-primary btn-block" type="submit">Iniciar Sesión</button>
      </form>
      <a href="#ventana0" id="boton"  data-toggle="modal"><button type="button"  class="btn btn-danger btn-block" aria-hidden="true" formnovalidate>Regístrate</button></a>
    
  </div>
</div>
<div class="modal fade" id="ventana0">
    <div class="modal-dialog">
      <div class="modal-content">
        <!--Header del Modal-->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        </div>
        <!--Body del Modal con formulario-->
        <form method="POST" action="usuarios/saveU.php" class="" name="f1">
          <div class="modal-body">
            <form name="nuevoUsuario" id="nuevoUsuario" action="save.php" method="post">
                <div class="row">
                  <div class="col-lg-6 form-group">
                              <label for="nombre">Nombre</label>
                              <input type="text" name="nombre" id="nombre" class="form-control" required>
                          </div>
                      <div class="col-lg-6 form-group">
                              <label for="apellido">Apellido</label>
                              <input type="text" name="apellido" id="apellido" class="form-control" required>
                          </div>
                </div>
                <div class="row">
                  <div class="col-lg-12 form-group">
                              <label for="email">Correo</label>
                              <input type="email" name="email" id="email" class="form-control" required>
                          </div>
                </div>
                        <div class="row">
                          <div class="col-lg-6 form-group">
                              <label for="pass1">Contraseña</label>
                              <input type="password" name="pass1" id="pass1" class="form-control" required>
                          </div>
                          <div class="col-lg-6 form-group">
                              <label for="pass2">Repite la Contraseña</label>
                              <input type="password" name="pass2" id="pass2" class="form-control" required>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-lg-6 form-group">
                              <label for="region">Estado</label>
                              <select name="region" id="region" class="form-control" required>
                                <option value="">Seleccionar</option>
                                <option value="Amazonas">Amazonas</option>
                                <option value="Anzoategui">Anzoátegui</option>
                                <option value="Apure">Apure</option>
                                <option value="Aragua">Aragua</option>
                                <option value="Barinas">Barinas</option>
                                <option value="Bolivar">Bolívar</option>
                                <option value="Carabobo">Carabobo</option>
                                <option value="Cojedes">Cojedes</option>
                                <option value="Delta Amacuro">Delta Amacuro</option>
                                <option value="Distrito Capital">Distrito Capital</option>
                                <option value="Falcon">Falcón</option>
                                <option value="Guarico">Guárico</option>
                                <option value="Lara">Lara</option>
                                <option value="Merida">Mérida</option>
                                <option value="Miranda">Miranda</option>
                                <option value="Monagas">Monagas</option>
                                <option value="Nueva Esparta">Nueva Esparta</option>
                                <option value="Portuguesa">Portuguesa</option>
                                <option value="Sucre">Sucre</option>
                                <option value="Tachira">Táchira</option>
                                <option value="Trujillo">Trujillo</option>
                                <option value="Vargas">Vargas</option>
                                <option value="Yaracuy">Yaracuy</option>
                                <option value="Zulia">Zulia</option>
                              </select>
                          </div><!-- select de la region -->
                          <div class="col-lg-6 form-group">
                            <label for="nacimiento_lugar">Ciudad</label>
                            <input type="text" name="nacimiento_lugar" id="nacimiento_lugar" class="form-control" required>
                        </div>
                      </div>
                      <div class="row">
                        
                        <div class="col-lg-12 form-group">
                          <label for="nacimiento_fecha">Fecha de Nacimiento</label>
                          <input type="date" name="nacimiento_fecha" id="nacimiento_fecha" class="form-control" min="1960-01-01" max="2003-12-31" required>
                      </div>
                      </div>
          <!--Footer del modal-->
          <div class="modal-footer">
            <button type="button" class="btn btn-success" aria-hidden="true"  onClick="comprobarClave()">Enviar</button>
            <button type="button" data-dismiss="modal" class="btn btn-light" aria-hidden="true">Cancelar</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
  <script type="text/javascript">
  function comprobarClave(){ 
    pass1 = document.f1.pass1.value 
    pass2 = document.f1.pass2.value 

      if (pass1 == pass2) 
          document.f1.submit() 
      else 
          alert("Las dos claves son distintas, no podemos actualizar") 
  } 
    </script>
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script defer src="js/all.js"></script> <!--load all styles -->
    <!-- Custom scripts for this template -->
    <script src="js/clean-blog.min.js"></script>
  </body>