<?php
include"../header.php"; 
?>
    <!-- Main Content-->
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <h1>INSTRUCCIONES:</h1>
          <p>
            lee cada una de las afirmaciones. Si expresan características fuertes en tu persona y 
            te parece que la afirmación es veraz entonces marca la casilla en la parte inferior de la pregunta
          </p>
          <div class="form-group">
            <form action="resultado.php" method="post">
              <ol>
                <li for="1" class="form-check-label col-lg-10" for="materialChecked2">
                  <span>Prefiero hacer un mapa que explicarle a alguien como tiene que llegar.</span>
                  <br>
                  <input type="checkbox" class="form-check-input col-lg-2" value="1" id="1" name="1">
                  <br>
                </li>
                <li for="2" class="form-check-label col-lg-10" for="materialChecked2">
                  <span>Si estoy enojado(a) o contento (a) generalmente sé exactamente por qué.</span>
                  <br>
                  <input type="checkbox" class="form-check-input col-lg-2" value="1" id="2" name="2">
                  <br>
                </li>
                <li for="3" class="form-check-label col-lg-10" for="materialChecked2">
                  <span>Sé tocar (o antes sabía tocar) un instrumento musical.</span>
                  <br>
                  <input type="checkbox" class="form-check-input col-lg-2" value="1" id="3" name="3">
                  <br>
                </li>
                <li for="4" class="form-check-label col-lg-10" for="materialChecked2">
                  <span>Asocio la música con mis estados de ánimo.</span>
                  <br>
                  <input type="checkbox" class="form-check-input col-lg-2" value="1" id="4" name="4">
                  <br>
                </li>
                <li for="5" class="form-check-label col-lg-10" for="materialChecked2">
                  <span>Puedo sumar o multiplicar mentalmente con mucha rapidez.</span>
                  <br>
                  <input type="checkbox" class="form-check-input col-lg-2" value="1" id="5" name="5">
                  <br>
                </li>
                <li for="6" class="form-check-label col-lg-10" for="materialChecked2">
                  <span>Puedo ayudar a un amigo a manejar sus sentimientos porque yo lo pude hacer antes en relación a sentimientos parecidos.</span>
                  <br>
                  <input type="checkbox" class="form-check-input col-lg-2" value="1" id="6" name="6">
                  <br>
                </li>
                <li for="7" class="form-check-label col-lg-10" for="materialChecked2">
                  <span>Me gusta trabajar con calculadoras y computadores.</span>
                  <br>
                  <input type="checkbox" class="form-check-input col-lg-2" value="1" id="7" name="7">
                  <br>
                </li>
                <li for="8" class="form-check-label col-lg-10" for="materialChecked2">
                  <span>.Aprendo rápido a bailar un ritmo nuevo.</span>
                  <br>
                  <input type="checkbox" class="form-check-input col-lg-2" value="1" id="8" name="8">
                  <br>
                </li>
                <li for="9" class="form-check-label col-lg-10" for="materialChecked2">
                  <span>No me es difícil decir lo que pienso en el curso de una discusión o debate.</span>
                  <br>
                  <input type="checkbox" class="form-check-input col-lg-2" value="1" id="9" name="9">
                  <br>
                </li>
                <li for="10" class="form-check-label col-lg-10" for="materialChecked2">
                  <span>Disfruto de una buena charla, discurso o sermón</span>
                  <br>
                  <input type="checkbox" class="form-check-input col-lg-2" value="1" id="10" name="10">
                  <br>
                </li>
                <li for="11" class="form-check-label col-lg-10" for="materialChecked2">
                  <span>Siempre distingo el norte del sur, esté donde esté.</span>
                  <br>
                  <input type="checkbox" class="form-check-input col-lg-2" value="1" id="11" name="11">
                  <br>
                </li>
                <li for="12" class="form-check-label col-lg-10" for="materialChecked2">
                  <span>Me gusta reunir grupos de personas en una fiesta o en un evento especial.</span>
                  <br>
                  <input type="checkbox" class="form-check-input col-lg-2" value="1" id="12" name="12">
                  <br>
                </li>
                <li for="13" class="form-check-label col-lg-10" for="materialChecked2">
                  <span>La vida me parece vacía sin música</span>
                  <br>
                  <input type="checkbox" class="form-check-input col-lg-2" value="1" id="13" name="13">
                  <br>
                </li>
                <li for="14" class="form-check-label col-lg-10" for="materialChecked2">
                  <span>Siempre entiendo los gráficos que vienen en las instrucciones de equipos o instrumentos.</span>
                  <br>
                  <input type="checkbox" class="form-check-input col-lg-2" value="1" id="14" name="14">
                  <br>
                </li>
                <li for="15" class="form-check-label col-lg-10" for="materialChecked2">
                  <span>Me gusta hacer rompecabezas y entretenerme con juegos electrónicos</span>
                  <br>
                  <input type="checkbox" class="form-check-input col-lg-2" value="1" id="15" name="15">
                  <br>
                </li>
                <li for="16" class="form-check-label col-lg-10" for="materialChecked2">
                  <span>Me fue fácil aprender a andar en bicicleta. (o patines)</span>
                  <br>
                  <input type="checkbox" class="form-check-input col-lg-2" value="1" id="16" name="16">
                  <br>
                </li>
                <li for="17" class="form-check-label col-lg-10" for="materialChecked2">
                  <span>Me enojo cuando oigo una discusión o una afirmación que parece ilógica.</span>
                  <br>
                  <input type="checkbox" class="form-check-input col-lg-2" value="1" id="17" name="17">
                  <br>
                </li>
                <li for="18" class="form-check-label col-lg-10" for="materialChecked2">
                  <span>Soy capaz de convencer a otros que sigan mis planes.</span>
                  <br>
                  <input type="checkbox" class="form-check-input col-lg-2" value="1" id="18" name="18">
                  <br>
                </li>
                <li for="19" class="form-check-label col-lg-10" for="materialChecked2">
                  <span>Tengo buen sentido de equilibrio y coordinación.</span>
                  <br>
                  <input type="checkbox" class="form-check-input col-lg-2" value="1" id="19" name="19">
                  <br>
                </li>
                <li for="20" class="form-check-label col-lg-10" for="materialChecked2">
                  <span>Con frecuencia veo configuraciones y relaciones entre números con más rapidez y facilidad que otros.</span>
                  <br>
                  <input type="checkbox" class="form-check-input col-lg-2" value="1" id="20" name="20">
                  <br>
                </li>
                <li for="21" class="form-check-label col-lg-10" for="materialChecked2">
                  <span>Me gusta construir modelos ( o hacer esculturas)</span>
                  <br>
                  <input type="checkbox" class="form-check-input col-lg-2" value="1" id="21" name="21">
                  <br>
                </li>
                <li for="22" class="form-check-label col-lg-10" for="materialChecked2">
                  <span>Tengo agudeza para encontrar el significado de las palabras</span>
                  <br>
                  <input type="checkbox" class="form-check-input col-lg-2" value="1" id="22" name="22">
                  <br>
                </li>
                <li for="23" class="form-check-label col-lg-10" for="materialChecked2">
                  <span>Puedo mirar un objeto de una manera y con la misma facilidad verlo.</span>
                  <br>
                  <input type="checkbox" class="form-check-input col-lg-2" value="1" id="23" name="23">
                  <br>
                </li>
                <li for="24" class="form-check-label col-lg-10" for="materialChecked2">
                  <span>Con frecuencia hago la conexión entre una pieza de música y algún evento de mi vida</span>
                  <br>
                  <input type="checkbox" class="form-check-input col-lg-2" value="1" id="24" name="24">
                  <br>
                </li>
                <li for="25" class="form-check-label col-lg-10" for="materialChecked2">
                  <span>Me gusta trabajar con números y figuras.</span>
                  <br>
                  <input type="checkbox" class="form-check-input col-lg-2" value="1" id="25" name="25">
                  <br>
                </li>
                <li for="26" class="form-check-label col-lg-10" for="materialChecked2">
                  <span>Me gusta sentarme silenciosamente y reflexionar sobre mis sentimientos íntimos.</span>
                  <br>
                  <input type="checkbox" class="form-check-input col-lg-2" value="1" id="26" name="26">
                  <br>
                </li>
                <li for="27" class="form-check-label col-lg-10" for="materialChecked2">
                  <span>Con sólo mirar la forma de construcciones y estructuras me siento a gusto.</span>
                  <br>
                  <input type="checkbox" class="form-check-input col-lg-2" value="1" id="27" name="27">
                  <br>
                </li>
                <li for="28" class="form-check-label col-lg-10" for="materialChecked2">
                  <span>Me gusta tararear, silbar y cantar en la ducha o cuando estoy solo(a).</span>
                  <br>
                  <input type="checkbox" class="form-check-input col-lg-2" value="1" id="28" name="28">
                  <br>
                </li>
                <li for="29" class="form-check-label col-lg-10" for="materialChecked2">
                  <span>Soy bueno(a) para el atletismo.</span>
                  <br>
                  <input type="checkbox" class="form-check-input col-lg-2" value="1" id="29" name="29">
                  <br>
                </li>
                <li for="30" class="form-check-label col-lg-10" for="materialChecked2">
                  <span>Me gusta escribir cartas detalladas a mis amigos.</span>
                  <br>
                  <input type="checkbox" class="form-check-input col-lg-2" value="1" id="30" name="30">
                  <br>
                </li>
                <li for="31" class="form-check-label col-lg-10" for="materialChecked2">
                  <span>Generalmente me doy cuenta de la expresión que tengo en la cara.</span>
                  <br>
                  <input type="checkbox" class="form-check-input col-lg-2" value="1" id="31" name="31">
                  <br>
                </li>
                <li for="32" class="form-check-label col-lg-10" for="materialChecked2">
                  <span>Me doy cuenta de las expresiones en la cara de otras personas.</span>
                  <br>
                  <input type="checkbox" class="form-check-input col-lg-2" value="1" id="32" name="32">
                  <br>
                </li>
                <li for="33" class="form-check-label col-lg-10" for="materialChecked2">
                  <span>Me mantengo "en contacto" con mis estados de ánimo. No me cuesta identificarlos.</span>
                  <br>
                  <input type="checkbox" class="form-check-input col-lg-2" value="1" id="33" name="33">
                  <br>
                </li>
                <li for="34" class="form-check-label col-lg-10" for="materialChecked2">
                  <span>Me doy cuenta de los estados de ánimo de otros.</span>
                  <br>
                  <input type="checkbox" class="form-check-input col-lg-2" value="1" id="34" name="34">
                  <br>
                </li>
                <li for="35" class="form-check-label col-lg-10" for="materialChecked2">
                  <span>Me doy cuenta bastante bien de lo que otros piensan de mí. </span>
                  <br>
                  <input type="checkbox" class="form-check-input col-lg-2" value="1" id="35" name="35">
                  <br>
                </li>
              </ol>
              <button type="submit" class="btn btn-primary" >Terminar test</button>
            </form>  
          </div>
        </div>
      </div>
    </div>

    <hr>
</body>
<?php
include"../footer.php"; 
?>