<?php
include"connection.php";
include"sessionsheader.php";
$nivel_us=$_SESSION['nivel_us'];
$idU=$_SESSION['idU'];
/*$idU=$_POST['id'];

$sqlUsuario = "SELECT nivel_id AS niv FROM usuario WHERE id='$id'";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0) {
    // output data of each row
  while($row = mysqli_fetch_assoc($result)){ 
    $nivelU=$row['niv'];
  }
}*/
?>
<!DOCTYPE html>
<html lang="es">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>VOCATEST</title>
    <link rel="icon" href="../img/icono.ico">
    <!-- Bootstrap core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="../css/clean-blog.min.css" rel="stylesheet">
    <!--FontAwesome-->
    <link href="../css/all.css" rel="stylesheet"> <!--load all styles -->

  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand" href="index.php">VOCATEST</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            
            <?php 
            if ($nivel_us==1){
              echo'<li class="nav-item">
                      <a class="nav-link" href="../estadisticas/index.php">Estadísticas</a>
                  </li>';
            }
            ?>
              
            
            <li class="nav-item">
              <a class="nav-link" href="../inteligencias/index.php">Inteligencias</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../test/index.php">Realizar Test</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../usuarios/perfil.php">Perfil</a>
            </li>
            <?php 
            if ($nivel_us==1){
              echo'<li class="nav-item">
                      <a class="nav-link" href="../usuarios/index.php">Gestión de Usuarios</a>
                    </li>';
            }
            ?>
            
          </ul>
        </div>
      </div>
    </nav>

    <!-- Page Header -->
    <header class="masthead" style="background-image: url('../img/home-bg.jpg')">
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto">
            <div class="site-heading">
              <img class="img-responsive" src="../img/vocatest.png">
              <span class="subheading">Tu Test Vocacional estudiantil dentro de la Universidad José Antonio Páez</span>
            </div>
          </div>
        </div>
      </div>
    </header>
