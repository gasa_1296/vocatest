<?php
include"../header.php";
?>
<body>
    <!-- Page Content -->
    <div class="container">
    	<div class="row">
	        <div class="col-md-8">
	        	<h1 class="my-4">Lista de Usuarios
	            	<small></small>
	          	</h1>
	        </div>
	        <div class="container-fluid">           
	            <table class="table table-fixed table-hover table-border table-hover table-striped table-responsive" >
	                <thead>
	                    <tr>
	                    	<th class="col-xs-2">Nombre</th>
	                        <th class="col-xs-2">Apellido</th>
	                        <th class="col-xs-2">Usuario</th>
	                        <th class="col-xs-2">Nivel</th>
	                        <th class="col-xs-2">Acción</th>
	                    </tr>
	                </thead>
	                <tbody>
<?php
$sql = "SELECT lo1.id,lo1.nombre as mauricio,lo1.apellido,lo1.email,lo1.estado,lo2.nombre FROM usuario lo1 INNER JOIN nivel lo2 ON lo1.nivel_id=lo2.id WHERE estado=1";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0) {
    // output data of each row
    while($row = mysqli_fetch_assoc($result)) {
        $modificar='<a data-toggle="popover" data-container="body" data-trigger="hover" data-placement="top" data-content="Editar este campo" class="btn btn-warning" href="edit.php?id=' . $row['id'] . '" role="button"><i class="far fa-edit"></i></a>';
        $eliminar='<a href="delete.php?id=' . $row['id'] . '"><button type="button" class="btn btn-danger" aria-hidden="true"><i class="far fa-trash-alt"></i></button></a>';

        echo"<tr>           
            	<td class='col-xs-2' id='acc'>".$row[mauricio]."</td>
                <td class='col-xs-2' id='acc'>".$row[apellido]."</td> 
                <td class='col-xs-2' id='acc'>".$row[email]."</td>
                <td class='col-xs-2' id='acc'>".$row[nombre]."</td>
              	<td class='col-xs-2' id='acc'>".$modificar."  ".$eliminar."</td>
            </tr>";
            //ahora viene un echo del modal de borrado
        /*echo'<div class="modal fade" id="#ventanaDel' . $row['id'] . '">
				<div class="modal-dialog">
					<div class="modal-content">
						<!--Header del Modal-->
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h3>Eliminar Usuario</h3>
						</div>
						<!--Body del Modal-->
						<div class="modal-body">
						</div>
					</div>
				</div>
			</div>';*/
    }
} else {
    echo "<td>0 results</td>";
}
?>
        			</tbody>
        		</table> 
			</div>
   		</div><!-- /.row -->
   		<div class="row">
   			<a href="new.php"><button class="btn btn-secondary">Nuevo Usuario</button></a>
   		</div>
   		<br>
    </div><!-- /.container -->
    <script type="text/javascript"></script>
</body>
<?php
include"../footer.php";
?>