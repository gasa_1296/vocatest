<?php
include"../header.php";
?>
<body>
    <!-- Page Content -->
    <div class="container"><br>
    	<div class="row">
	        <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3>Nuevo Usuario</h3>
                </div> 
            	<div class="panel-body">
      				<form name="nuevoUsuario" id="nuevoUsuario" action="save.php" method="post">
      					<div class="row">
      						<div class="col-lg-6 form-group">
	                            <label for="nombre">Nombre</label>
	                            <input type="text" name="nombre" id="nombre" class="form-control" required>
	                        </div>
	                		<div class="col-lg-6 form-group">
	                            <label for="apellido">Apellido</label>
	                            <input type="text" name="apellido" id="apellido" class="form-control" required>
	                        </div>
      					</div>
      					<div class="row">
      						<div class="col-lg-12 form-group">
	                            <label for="email">Correo</label>
	                            <input type="email" name="email" id="email" class="form-control" required>
	                        </div>
      					</div>
                        <div class="row">
                        	<div class="col-lg-6 form-group">
	                            <label for="pass1">Contraseña</label>
	                            <input type="password" name="pass1" id="pass1" class="form-control" required>
	                        </div>
	                        <div class="col-lg-6 form-group">
	                            <label for="pass2">Repite la Contraseña</label>
	                            <input type="password" name="pass2" id="pass2" class="form-control" required>
	                        </div>
                        </div>
                        <div class="row">
                        	<div class="col-lg-6 form-group">
	                            <label for="nivel_id">Nivel</label>
	                            <select name="nivel_id" id="nivel_id" class="form-control" required>
	                            	<option value="">Seleccionar</option>
	                            	<?php
	                                $sql = "SELECT id as num, nombre as niv FROM nivel ";
	                                $result = mysqli_query($conn, $sql);
	                                if (mysqli_num_rows($result) > 0) {
	                                    // output data of each row
	                                    while($row = mysqli_fetch_assoc($result)) {
	                                        echo "<option value='".$row['num']."'>".$row['niv']."</option>";
	                                    }
	                                }
	                                ?>
	                            </select>
	                        </div>
                        	<div class="col-lg-6 form-group">
	                            <label for="region">Estado</label>
	                            <select name="region" id="region" class="form-control" required>
	                            	<option value="">Seleccionar</option>
	                            	<option value="Amazonas">Amazonas</option>
	                            	<option value="Anzoategui">Anzoátegui</option>
	                            	<option value="Apure">Apure</option>
	                            	<option value="Aragua">Aragua</option>
	                            	<option value="Barinas">Barinas</option>
	                            	<option value="Bolivar">Bolívar</option>
	                            	<option value="Carabobo">Carabobo</option>
	                            	<option value="Cojedes">Cojedes</option>
	                            	<option value="Delta Amacuro">Delta Amacuro</option>
	                            	<option value="Distrito Capital">Distrito Capital</option>
	                            	<option value="Falcon">Falcón</option>
	                            	<option value="Guarico">Guárico</option>
	                            	<option value="Lara">Lara</option>
	                            	<option value="Merida">Mérida</option>
	                            	<option value="Miranda">Miranda</option>
	                            	<option value="Monagas">Monagas</option>
	                            	<option value="Nueva Esparta">Nueva Esparta</option>
	                            	<option value="Portuguesa">Portuguesa</option>
	                            	<option value="Sucre">Sucre</option>
	                            	<option value="Tachira">Táchira</option>
	                            	<option value="Trujillo">Trujillo</option>
	                            	<option value="Vargas">Vargas</option>
	                            	<option value="Yaracuy">Yaracuy</option>
	                            	<option value="Zulia">Zulia</option>
	                            </select>
	                        </div><!-- select de la region -->
	                    </div>
	                    <div class="row">
	                    	<div class="col-lg-6 form-group">
		                    	<label for="nacimiento_lugar">Ciudad</label>
		                    	<input type="text" name="nacimiento_lugar" id="nacimiento_lugar" class="form-control" required>
	                    	</div>
	                    	<div class="col-lg-12 form-group">
			                    <label for="nacimiento_fecha">Fecha de Nacimiento</label>
			                    <input type="date" name="nacimiento_fecha" id="nacimiento_fecha" class="form-control" min="1960-01-01" max="2003-12-31" required>
			                </div>
	                    </div>
                        <div class="row">
                        	<div class="col-xs-3 col-lg-3">
	                           <button type="button" class="btn btn-outline-primary" onClick="comprobarClave()">Guardar</button>
	                     	</div>
	                     	<div class="col-xs-1 col-lg-1"></div>
	                     	<br class="hidden-sm-up">
	                     	<div class="col-xs-3 col-lg-3">
	                     		<a href="index.php"><button type="button"  class="btn btn-outline-secondary" formnovalidate>Cancelar</button></a>
	                     	</div>
                        </div>
                    </form>
                </div>		
            </div>
		</div>
    </div><!-- /.container -->
    <br><br><br>
    <script type="text/javascript">
      function comprobarClave(){ 
    pass1 = document.f1.pass1.value 
    pass2 = document.f1.pass2.value 

    if (pass1 == pass2) 
        document.f1.submit() 
    else 
        alert("Las dos claves son distintas, no podemos actualizar") 
} 
    </script>
</body>
<?php
include"../footer.php";
?>