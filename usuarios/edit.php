<?php
include"../header.php";
$id=@$_GET['id'];

$sql = "SELECT * FROM usuario WHERE id='$id'";
$result = mysqli_query($conn, $sql);


if (mysqli_num_rows($result) > 0) {
    // output data of each row
    while($row = mysqli_fetch_assoc($result)) {
    	$nombre=$row['nombre'];
		$apellido=$row['apellido'];
		$email=$row['email'];
		$pass=$row['pass'];
		$nivel_id=$row['nivel_id'];
		$region=$row['region'];
		$nacimiento_fecha=$row['nacimiento_fecha'];
		$nacimiento_lugar=$row['nacimiento_lugar'];
    }
}
?>
<body>
    <!-- Page Content -->
    <div class="container"><br>
    	<div class="row">
	        <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3>Nuevo Usuario</h3>
                </div> 
            	<div class="panel-body">
      				<form name="nuevoUsuario" id="nuevoUsuario" action="update.php" method="post">
      					<div class="row">
      						<div class="col-lg-6 form-group">
      							<input type="hidden" name="id" value="<?php echo $id;?>">
	                            <label for="nombre">Nombre</label>
	                            <input type="text" name="nombre" id="nombre" class="form-control" value="<?php echo $nombre; ?>" required>
	                        </div>
	                		<div class="col-lg-6 form-group">
	                            <label for="apellido">Apellido</label>
	                            <input type="text" name="apellido" id="apellido" class="form-control" value="<?php echo $apellido; ?>" required>
	                        </div>
      					</div>
      					<div class="row">
      						<div class="col-lg-12 form-group">
	                            <label for="email">Correo</label>
	                            <input type="email" name="email" id="email" class="form-control" value="<?php echo $email; ?>" required>
	                        </div>
      					</div>
                        <div class="row">
                        	<div class="col-lg-6 form-group">
	                            <label for="pass1">Contraseña</label>
	                            <input type="password" name="pass1" id="pass1" class="form-control" value="<?php echo $pass; ?>" required>
	                        </div>
	                        <div class="col-lg-6 form-group">
	                            <label for="pass2">Repite la Contraseña</label>
	                            <input type="password" name="pass2" id="pass2" class="form-control" value="<?php echo $pass; ?>" required>
	                        </div>
                        </div>
                        <div class="row">
                        	<div class="col-lg-6 form-group">
	                            <label for="nivel_id">Nivel</label>
	                            <select name="nivel_id" id="nivel_id" class="form-control" required>
	                            	<option value="">Seleccionar</option>
	                            	<?php
	                                $sql1 = "SELECT id AS num, nombre AS niv FROM nivel ";
	                                $result = mysqli_query($conn, $sql1);
	                                if (mysqli_num_rows($result) > 0) {
	                                    // output data of each row
	                                    while($row = mysqli_fetch_assoc($result)) {

	                                    	if($row['num']==$nivel_id){
	                                    		echo "<option value='".$row['id']."' selected>".$row['niv']."</option>";
	                                    	}else{
	                                    		echo "<option value='".$row['id']."'>".$row['niv']."</option>";
	                                    	}
	              							
	                                    }
	                                }
	                                ?>
	                            </select>
	                        </div>
                        	<div class="col-lg-6 form-group">
	                            <label for="region">Estado</label>
	                            <select name="region" id="region" class="form-control" required>
	                            	<option value="" <?php if ($region==''){echo'selected';}?>>Seleccionar</option>
	                            	<option value="Amazonas" <?php if ($region=='Amazonas'){echo'selected';}?>>Amazonas</option>
	                            	<option value="Anzoategui" <?php if ($region=='Anzoategui'){echo'selected';}?>>Anzoátegui</option>
	                            	<option value="Apure" <?php if ($region=='Apure'){echo'selected';}?>>Apure</option>
	                            	<option value="Aragua" <?php if ($region=='Aragua'){echo'selected';}?>>Aragua</option>
	                            	<option value="Barinas" <?php if ($region=='Barinas'){echo'selected';}?>>Barinas</option>
	                            	<option value="Bolivar" <?php if ($region=='Bolivar'){echo'selected';}?>>Bolívar</option>
	                            	<option value="Carabobo" <?php if ($region=='Carabobo'){echo'selected';}?>>Carabobo</option>
	                            	<option value="Cojedes" <?php if ($region=='Cojedes'){echo'selected';}?>>Cojedes</option>
	                            	<option value="Delta Amacuro" <?php if ($region=='Delta Amacuro'){echo'selected';}?>>Delta Amacuro</option>
	                            	<option value="Distrito Capital" <?php if ($region=='Distrito Capital'){echo'selected';}?>>Distrito Capital</option>
	                            	<option value="Falcon" <?php if ($region=='Falcon'){echo'selected';}?>>Falcón</option>
	                            	<option value="Guarico" <?php if ($region=='Guarico'){echo'selected';}?>>Guárico</option>
	                            	<option value="Lara" <?php if ($region=='Lara'){echo'selected';}?>>Lara</option>
	                            	<option value="Merida" <?php if ($region=='Merida'){echo'selected';}?>>Mérida</option>
	                            	<option value="Miranda" <?php if ($region=='Miranda'){echo'selected';}?>>Miranda</option>
	                            	<option value="Monagas" <?php if ($region=='Monagas'){echo'selected';}?>>Monagas</option>
	                            	<option value="Nueva Esparta" <?php if ($region=='Nueva Esparta'){echo'selected';}?>>Nueva Esparta</option>
	                            	<option value="Portuguesa" <?php if ($region=='Portuguesa'){echo'selected';}?>>Portuguesa</option>
	                            	<option value="Sucre" <?php if ($region=='Sucre'){echo'selected';}?>>Sucre</option>
	                            	<option value="Tachira" <?php if ($region=='Tachira'){echo'selected';}?>>Táchira</option>
	                            	<option value="Trujillo" <?php if ($region=='Trujillo'){echo'selected';}?>>Trujillo</option>
	                            	<option value="Vargas" <?php if ($region=='Vargas'){echo'selected';}?>>Vargas</option>
	                            	<option value="Yaracuy" <?php if ($region=='Yaracuy'){echo'selected';}?>>Yaracuy</option>
	                            	<option value="Zulia" <?php if ($region=='Zulia'){echo'selected';}?>>Zulia</option>
	                            </select>
	                        </div>
	                       	
	                    </div>
	                    <div class="row">
	                    	<div class="col-lg-6 form-group">
		                    	<label for="nacimiento_lugar">Ciudad</label>
		                    	<input type="text" name="nacimiento_lugar" id="nacimiento_lugar" class="form-control" value="<?php echo $nacimiento_lugar; ?>" required>
	                    	</div>
	                    	<div class="col-lg-12 form-group">
			                    <label for="nacimiento_fecha">Fecha de Nacimiento</label>
			                    <input type="date" name="nacimiento_fecha" id="nacimiento_fecha" class="form-control" value="<?php echo $nacimiento_fecha; ?>" min="1960-01-01" max="2003-01-01" required>
			                </div>
	                    </div>
                        <div class="row">
                        	<div class="col-xs-3 col-lg-3">
	                           <button type="button" class="btn btn-outline-primary" onClick="comprobarClave()">Guardar</button>
	                     	</div>
	                     	<div class="col-xs-1 col-lg-1"></div>
	                     	<br class="hidden-sm-up">
	                     	<div class="col-xs-3 col-lg-3">
	                     		<a href="index.php"><button type="button"  class="btn btn-outline-secondary" formnovalidate>Cancelar</button></a>
	                     	</div>
                        </div>
           
                    </form>
                </div>		
            </div>
		</div>
    </div><!-- /.container -->
    <br><br><br>
    <script type="text/javascript">
      function comprobarClave(){ 
    pass1 = document.f1.pass1.value 
    pass2 = document.f1.pass2.value 

    if (pass1 == pass2) 
        document.f1.submit() 
    else 
        alert("Las dos claves son distintas, no podemos actualizar") 
} 
    </script>
</body>
<?php
include"../footer.php";
?>