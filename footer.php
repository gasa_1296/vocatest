<!-- Footer -->
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto">
            <p class="copyright text-muted">Vocatest &copy; Universidad José Antonio Páez, 2018</p>
          </div>
        </div>
      </div>
    <!-- Bootstrap core JavaScript -->
    <script src="../vendor/jquery/jquery.min.js"></script>
    <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script defer src="../js/all.js"></script> <!--load all styles -->
    <!-- Custom scripts for this template -->
    <script src="../js/clean-blog.min.js"></script>
</footer>
</html>