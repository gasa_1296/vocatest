<?php
include"../header.php"; 
?>
<div class="container">
    <div class="row">
        <div class="col-lg-10 col-md-12 mx-auto">
            <?php
            $sql="SELECT * from usuario where estado=1";
            $result = mysqli_query($conn, $sql);
            $count = mysqli_num_rows($result);
            if($count > 0){
                echo "<span><strong>Numero de usuarios registrados: </strong>".$count."</span>";
            }            
            ?>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-lg-10 col-md-12 mx-auto">
            <?php
            $sql="SELECT * from prueba";
            $result = mysqli_query($conn, $sql);
            if($result){
                echo "<span><strong>Numero de pruebas realizadas: </strong>".round(mysqli_num_rows($result),3)."</span>";
            }            
            ?>
        </div>
        <div class="col-lg-10 col-md-12 mx-auto">
            <?php
            for($i=1; $i<8; $i++){
                $sql="SELECT * from prueba where inteligencia_id='$i'";
                $sql1="SELECT * from prueba";
                $sql2="SELECT nombre from inteligencia where id='$i'";
                $result = mysqli_query($conn, $sql);
                $result1 = mysqli_query($conn, $sql1);                
                $row = mysqli_fetch_assoc(mysqli_query($conn, $sql2));
                if($result && $result1){
                    $porcentaje = mysqli_num_rows($result)*100/mysqli_num_rows($result1);
                    echo "<span><strong>porcentaje de pruebas realizadas con una inteligencia dominante ".$row["nombre"].": <br></strong>".round($porcentaje,3)."</span><br>";
                }  
            }                      
            ?>
        </div>
    </div>
</div>
<?php
include"../footer.php"; 
?>