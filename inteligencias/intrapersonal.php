<?php
include"../header.php"; 
?>
    <!-- Main Content-->
    <div class="container">
      <div class="row">
        <div class="col-lg-10 col-md-10 mx-auto">
          <h1>INTELIGENCIA INTRAPERSONAL</h1>
          <p>
            La inteligencia intrapersonal se refiere a la autocomprensión, el acceso a la propia vida emocional, a la propia gama de 
            sentimientos, la capacidad de efectuar discriminaciones de estas emociones y finalmente ponerles nombre y recurrir a ellas 
            como medio de interpretar y orientar la propia conducta. Las personas que poseen una inteligencia intrapersonal notable, 
            poseen modelos viables y eficaces de sí mismos. Pero al ser esta forma de inteligencia la más privada de todas, requiere 
            otras formas expresivas para que pueda ser observada en funcionamiento.
            <br>
            La inteligencia interpersonal permite comprender y trabajar con los demás, la intrapersonal permite comprenderse mejor y 
            trabajar con uno mismo. En el sentido individual de uno mismo, es posible hallar una mezcla de componentes intrapersonales e 
            interpersonales. El sentido de uno mismo es una de las más notables invenciones humanas: simboliza toda la información posible 
            respecto a una persona y qué es. Se trata de una invención que todos los individuos construyen para sí mismos. 
            <br>
            entre las carreras afines tenemos:
          </p>
          <ul>
              <li>Ingenieria en Computacion</li>
          </ul>            
        </div>
      </div>
    </div>
    <hr>
</body>
<?php
include"../footer.php"; 
?>