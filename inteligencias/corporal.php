<?php
include"../header.php"; 
?>
    <!-- Main Content-->
    <div class="container">
      <div class="row">
        <div class="col-lg-10 col-md-10 mx-auto">
            <h1>INTELIGENCIA CORPORAL-CINESTESICA</h1>
            <p>
                La inteligencia corporal cinestésica o kinestésica es la capacidad de unir el cuerpo y la mente para lograr el perfeccionamiento 
                del desempeño físico. Comienza con el control de los movimientos automáticos y voluntarios, avanza hacia el empleo del cuerpo 
                de manera altamente diferenciada y competente. Permite al individuo manipular objetos y perfeccionar las habilidades 
                físicas. Se manifiesta en los atletas, los bailarines, los cirujanos y los artesanos.
                <br>
                También existe la habilidad cinestésica expresada en movimientos pequeños, por lo que podemos admirar esta capacidad 
                en personas que se dedican a la joyería, mecánicos o que se dedican al cultivo de distintas artesanías y trabajos manuales. 
                La escuela tradicional no le da suficiente importancia a este tipo de inteligencia, se le dedican una o dos horas 
                semanales a las actividades que la desarrollan y es una manera de formar socialización, la estimulación sensoriomotriz 
                no solo sirve a nivel físico sino que permite mayor desarrollo cognitivo. 
                <br>
                entre las carreras afines tenemos:
            </p>
            <ul>
                <li>Odontologia</li>
            </ul>
        </div>
      </div>
    </div>
    <hr>
</body>
<?php
include"../footer.php"; 
?>