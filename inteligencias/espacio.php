<?php
include"../header.php"; 
?>
    <!-- Main Content-->
    <div class="container">
      <div class="row">
        <div class="col-lg-10 col-md-10 mx-auto">
            <h1>INTELIGENCIA ESPACIAL</h1>
            <p>
                Percibir la realidad, apreciando tamaños, direcciones y relaciones espaciales. Reproducir mentalmente objetos que se 
                han observado. Reconocer el mismo objeto en diferentes circunstancias. La imagen queda tan fija que el individuo es capaz 
                de identificarla, independientemente del lugar, posición o situación en que el objeto se encuentre. Anticiparse a las 
                consecuencias de cambios espaciales, y adelantarse e imaginar o suponer cómo puede variar un objeto que sufre algún tipo 
                de cambio. Describir coincidencias o similitudes entre objetos que lucen distintos, identificar aspectos comunes o 
                diferencias en los objetos que se encuentran alrededor de un individuo. La inteligencia espacial es la inteligencia 
                de los arquitectos, pilotos, navegantes, jugadores de ajedrez, cirujanos, pintores, escultores, ingenieros, etc
                <br>
                entre las carreras afines tenemos:
            </p>
            <ul>
                <li>Ingenieria Mecanica</li>
                <li>Ingenieria Civil</li>
                <li>Ingenieria Industrial</li>
                <li>Arquitectura </li>
            </ul>            
        </div>
      </div>
    </div>
    <hr>
</body>
<?php
include"../footer.php"; 
?>