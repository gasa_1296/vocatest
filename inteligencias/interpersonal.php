<?php
include"../header.php"; 
?>
    <!-- Main Content-->
    <div class="container">
      <div class="row">
        <div class="col-lg-10 col-md-10 mx-auto">
            <h1>INTELIGENCIA INTERPERSONAL</h1>
            <p>
                La inteligencia interpersonal permite comprender a los demás y comunicarse con ellos, teniendo en cuenta sus diferentes 
                estados de ánimo, temperamentos, motivaciones y habilidades. Incluye la capacidad para establecer y mantener relaciones sociales 
                y para asumir diversos roles dentro de grupos, ya sea como un miembro más o como líder. Son aquellos individuos que poseen la llave 
                de las relaciones humanas, del sentido del humor: desde pequeños disfrutan de la interacción con amigos y compañeros 
                escolares, y en general no tienen dificultades para relacionarse con personas de otras edades diferentes a la suya.
                <br>
                Algunos presentan una sensibilidad especial para detectar los sentimientos de los demás, se interesan 
                por los diversos estilos culturales y las diferencias socioeconómicas de los grupos humanos. La mayoría de ellos influyen sobre 
                otros y gustan del trabajo grupal especialmente en proyectos colaborativos. Son capaces de ver distintos puntos de vista en cuanto 
                a cuestiones sociales o políticas, y aprecian valores y opiniones diferentes de las suyas. 
                <br>
                entre las carreras afines tenemos:
            </p>
            <ul>
                <li>Derecho</li>
                <li>Educacion</li>
                <li>Mercadeo</li>
            </ul>          
        </div>
      </div>
    </div>
    <hr>
</body>
<?php
include"../footer.php"; 
?>