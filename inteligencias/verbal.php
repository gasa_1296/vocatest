<?php
include"../header.php"; 
?>
    <!-- Main Content-->
<div class="container">
    <div class="row">
        <div class="col-lg-10 col-md-10 mx-auto">
            <h1>INTELIGENCIA VERBAL</h1>
            <p>
                Esta inteligencia incluye también la habilidad de usar efectivamente el lenguaje para expresarse retóricamente o tal vez 
                poéticamente. Esta inteligencia es normal en escritores, poetas, líderes carismáticos y otras profesiones que utilizan 
                habilidades como la de comunicarse. No obstante, no nos limita únicamente a la capacidad de comunicar, sino también a la de 
                vincular conceptos mediante símbolos o signos.
                <br>
                La inteligencia lingüística se reconoce como una actitud humana de largo estudio y se le atribuye "humana" 
                porque permite diferenciar principalmente a los seres humanos de la especie animal por poder mantener conversaciones 
                comunicativas entre personas y por presentar una mayor capacidad de atención a la hora de percibir información de aquel 
                que la transmite.
                <br>
                entre las carreras afines tenemos:
            </p>
            <ul>
                <li>Derecho</li>
                <li>Educacion</li>
                <li>Mercadeo</li>
            </ul>            
        </div>
      </div>
    </div>
    <hr>
</body>
<?php
include"../footer.php"; 
?>