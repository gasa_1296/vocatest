<?php
include"../header.php"; 
?>
    <!-- Main Content-->
    <div class="container">
      <div class="row">
        <div class="col-lg-10 col-md-10 mx-auto">
            <h1>INTELIGENCIA RITMICA-MUSICAL</h1>
            <p>
                Este tipo de inteligencia se relaciona con la capacidad de percibir, discriminar, transformar y expresarse mediante 
                las formas musicales. Una persona que posee inteligencia musical es aquella que disfruta siguiendo el compás 
                con su pie o con algún objeto rítmico, también se sienten atraídos incluso por los sonidos de la naturaleza y 
                diversos tipos de melodías. 
                <br>
                También puede estar relacionada con la inteligencia lingüística, con la inteligencia espacial y con la inteligencia corporal 
                cinética. La inteligencia musical también se hace evidente en el desarrollo lingüístico, por cuanto demanda del individuo 
                procesos mentales que involucran la categorización de referencias auditivas y su posterior asociación con preconceptos; 
                esto es, el desarrollo de una habilidad para retener estructuras lingüísticas y asimilarlas en sus realizaciones fonéticas.
                <br>
                entre las carreras afines tenemos:
            </p>
            <ul>
                <li>Derecho</li>
                <li>Educacion</li>
            </ul>           
        </div>
      </div>
    </div>
    <hr>
</body>
<?php
include"../footer.php"; 
?>