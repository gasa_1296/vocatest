<?php
include"../header.php"; 
?>
    <!-- Main Content-->
    <div class="container">
      <div class="row">
        <div class="col-lg-10 col-md-10 mx-auto">
            <h1>INTELIGENCIA LOGICO-MATEMATICA</h1>
            <p>
                Esta inteligencia permite resolver problemas de lógica y de matemática, y es fundamental en las personas de formación científica;
                en la antigua concepción "unitaria" de la inteligencia era la capacidad predominante. Las personas que tienen un nivel alto 
                en este tipo de inteligencia poseen sensibilidad para realizar esquemas y relaciones lógicas, afirmaciones, proposiciones, 
                funciones y otras abstracciones relacionadas. También se refiere a un alto razonamiento numérico, la capacidad de 
                resolución, comprensión y planteamiento de elementos aritméticos, en general en resolución de problemas
                <br>
                entre las carreras afines tenemos:         
            </p>   
            <ul>
                <li>Ingenieria en Computacion</li>
                <li>Ingenieria Mecanica</li>
                <li>Ingenieria Civil</li>
                <li>Ingenieria Industrial</li>
                <li>Ingenieria Electronica</li>
                <li>Ingenieria en Telecomunicaciones</li>
                <li>Contaduria</li>
                <li>Administracion</li>
            </ul>       
        </div>
      </div>
    </div>
    <hr>
</body>
<?php
include"../footer.php"; 
?>