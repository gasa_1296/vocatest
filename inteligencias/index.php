<?php
include"../header.php"; 
?>
    <!-- Main Content-->
    <div class="container">
      <div class="row">
        <div class="col-lg-10">
          <div class="post-preview">
            <a href="verbal.php">
              <h2 class="post-title">
                Inteligencia verbal
              </h2>
              <p>
                Esta inteligencia incluye también la habilidad de usar efectivamente el lenguaje para expresarse retóricamente o tal vez 
                poéticamente. Esta inteligencia es normal en escritores, poetas, líderes carismáticos y otras profesiones que utilizan 
                habilidades como la de comunicarse. No obstante, no nos limita únicamente a la capacidad de comunicar, sino también a la 
                de vincular conceptos mediante símbolos o signos. 
              </p>
            </a>
          </div>
        </div>
        <div class="col-lg-10">
          <div class="post-preview">
            <a href="logico.php">
              <h2 class="post-title">
                Inteligencia Logico-Matematica
              </h2>
              <p>
                Esta inteligencia permite resolver problemas de lógica y de matemática, y es fundamental en las personas de formación 
                científica; en la antigua concepción "unitaria" de la inteligencia era la capacidad predominante. Las personas que tienen
                 un nivel alto en este tipo de inteligencia poseen sensibilidad para realizar esquemas y relaciones lógicas, afirmaciones, 
                 proposiciones, funciones y otras abstracciones relacionadas.
              </p>             
            </a>
          </div>
        </div>
        <div class="col-lg-10">
          <div class="post-preview">
            <a href="espacio.php">
              <h2 class="post-title">
                Inteligencia Espacial
              </h2>
              <p>
                Este tipo de inteligencia se relaciona con la capacidad que tiene el individuo frente a aspectos como color, línea, 
                forma, figura, espacio, y la relación que existe entre ellos. Es además, la capacidad que tiene una persona para procesar 
                información en tres dimensiones. Las personas con marcada tendencia espacial tienden a pensar en imágenes y fotografías, visualizarlas, diseñarlas o dibujarlas. 
              </p>
            </a>
          </div>
        </div>
        <div class="col-lg-10">
          <div class="post-preview">
            <a href="corporal.php">
              <h2 class="post-title">
                Inteligencia Corporal
              </h2>
              <p>
                La inteligencia corporal cinestésica o kinestésica es la capacidad de unir el cuerpo y la mente para lograr el 
                perfeccionamiento del desempeño físico. Comienza con el control de los movimientos automáticos y voluntarios, 
                avanza hacia el empleo del cuerpo de manera altamente diferenciada y competente. 
                <br>
                Permite al individuo manipular objetos y perfeccionar las habilidades físicas.
              </p>
            </a>
          </div>
        </div>
        <div class="col-lg-10">
          <div class="post-preview">
            <a href="intrapersonal.php">
              <h2 class="post-title">
                Inteligencia Intrapersonal
              </h2>
              <p>
                La inteligencia intrapersonal se refiere a la autocomprensión, el acceso a la propia vida emocional, a la propia gama de 
                sentimientos, la capacidad de efectuar discriminaciones de estas emociones y finalmente ponerles nombre y recurrir a 
                ellas como medio de interpretar y orientar la propia conducta. Las personas que poseen una inteligencia intrapersonal 
                notable, poseen modelos viables y eficaces de sí mismos. Pero al ser esta forma de inteligencia la más privada de todas, 
                requiere otras formas expresivas para que pueda ser observada en funcionamiento. 
              </p>
            </a>
          </div>
        </div>
        <div class="col-lg-10">
          <div class="post-preview">
            <a href="interpersonal.php">
              <h2 class="post-title">
                Inteligencia Interpersonal
              </h2>
              <p>
                Es la que nos permite entender a los demás. La inteligencia interpersonal es mucho más importante en nuestra vida diaria que 
                la brillantez académica, porque es la que determina la elección de la pareja, los amigos y, en gran medida, nuestro éxito 
                en el trabajo o en el estudio. La inteligencia interpersonal se basa en el desarrollo de dos grandes tipos de capacidades, 
                la empatía y la capacidad de manejar las relaciones interpersonales
              </p>
            </a>
          </div>
        </div>
        <div class="col-lg-10">
          <div class="post-preview">
            <a href="musical.php">
              <h2 class="post-title">
                Inteligencia Ritmica
              </h2>
              <p>
                Este tipo de inteligencia se relaciona con la capacidad de percibir, discriminar, transformar y expresarse mediante 
                las formas musicales. Asimismo, esta inteligencia incluye las habilidades en el canto dentro de cualquier tecnicismo 
                y género musical, tocar un instrumento a la perfección y lograr con él una adecuada presentación, dirigir un conjunto, 
                ensamble, orquesta; componer (en cualquier modo y género) y tener apreciación musical. 
              </p>
            </a>
          </div>
        </div>
      </div>
    </div>
</body>
<?php
include"../footer.php"; 
?>